<?php

use app\models\Countries,
    yii\db\Migration;

/**
 * Class m180517_115349_create_table_countries
 */
class m180517_115349_create_table_countries extends Migration
{
    private $table = '{{%countries}}';
    private $optString = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
    private $filePath = "@app/migrations/data/cnt_codes.csv";

    public function safeUp()
    {
        $this->createTable($this->table, [
            'cnt_id' => $this->primaryKey(),
            'cnt_code' => $this->smallInteger(3)->unsigned()->notNull(),
            'cnt_title' => $this->string(100)->notNull(),
            'cnt_created' => $this->dateTime(),
        ], $this->optString);

        $this->createIndex('cnt_code', $this->table, 'cnt_code');

        echo PHP_EOL . "Fill table {$this->table} started ...";
        $fillResult = $this->fillTable();
        echo PHP_EOL . "Finished";
        return $fillResult;
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }

    private function fillTable(): bool
    {
        if (!is_file(Yii::getAlias($this->filePath))) {
            return false;
        }

        $dateTime = date("Y-m-d H:i:s");
        $dataForInsert = [];

        foreach (file(Yii::getAlias($this->filePath)) as $line) {
            $dataRow = explode(',', $line);
            $dataForInsert[] = [$dataRow[3], $dataRow[0], $dateTime];
        }

        try {
            Countries::getDb()->createCommand()->batchInsert(
                Countries::tableName(),
                ['cnt_code', 'cnt_title', 'cnt_created'],
                $dataForInsert
            )->execute();
        } catch (Exception $e) {
            return false;
        }

        return true;
    }
}
