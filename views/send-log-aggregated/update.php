<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SendLogAggregated */

$this->title = 'Update Send Log Aggregated: ' . $model->logag_id;
$this->params['breadcrumbs'][] = ['label' => 'Send Log Aggregateds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->logag_id, 'url' => ['view', 'id' => $model->logag_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="send-log-aggregated-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
