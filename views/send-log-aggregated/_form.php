<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SendLogAggregated */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="send-log-aggregated-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'usr_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cnt_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'logag_successed')->textInput() ?>

    <?= $form->field($model, 'logag_filed')->textInput() ?>

    <?= $form->field($model, 'logag_date')->textInput() ?>

    <?= $form->field($model, 'logag_created')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
