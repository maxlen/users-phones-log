<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SendLog;

/**
 * SendLogSearch represents the model behind the search form of `app\models\SendLog`.
 */
class SendLogSearch extends SendLog
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['log_id', 'usr_id', 'num_id', 'log_success'], 'integer'],
            [['log_message', 'log_created'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SendLog::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'log_id' => $this->log_id,
            'usr_id' => $this->usr_id,
            'num_id' => $this->num_id,
            'log_success' => $this->log_success,
            'log_created' => $this->log_created,
        ]);

        $query->andFilterWhere(['like', 'log_message', $this->log_message]);

        return $dataProvider;
    }
}
