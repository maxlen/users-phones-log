<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SendLog */

$this->title = 'Create Send Log';
$this->params['breadcrumbs'][] = ['label' => 'Send Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="send-log-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
