<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SendLogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="send-log-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'log_id') ?>

    <?= $form->field($model, 'usr_id') ?>

    <?= $form->field($model, 'num_id') ?>

    <?= $form->field($model, 'log_message') ?>

    <?= $form->field($model, 'log_success') ?>

    <?php // echo $form->field($model, 'log_created') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
