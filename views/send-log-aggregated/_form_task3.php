<?php

use app\models\Countries;
use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SendLogAggregated */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="send-log-aggregated-form">
    <?php
    $this->registerCss("
    .formBlock {
        width: 250px;
        float: left;
        margin-left: 10px;
    }
    
    .blockLeft {
        float: left;
        margin: 10px 0px 0px 10px;
    }
    ");

    $dateTimeWOptions = [
        'convertFormat' => true,
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-MM-dd',
            'todayHighlight' => true
        ]
    ];
    ?>
    <div style="margin: 5px; clear: both;">
        <?php ActiveForm::begin(['method' => 'get']); ?>

        <div class='formBlock'>Date: from
            <?php
            $options = array_merge($dateTimeWOptions, ['name' => 'SendLogTask3Search[dateFrom]']);

            if (!empty($dateFrom)) {
                $options['value'] = $dateFrom;
            }

            echo DateTimePicker::widget($options);
            ?>
        </div>
        <div class='formBlock'>to
            <?php
            $options = array_merge($dateTimeWOptions, ['name' => 'SendLogTask3Search[dateTo]']);

            if (!empty($dateTo)) {
                $options['value'] = $dateTo;
            }

            echo DateTimePicker::widget($options);
            ?>
        </div>
        <div class='formBlock' style='width: 150px;'>Country
            <?php
            echo Html::dropDownList(
                'SendLogTask3Search[cnt_id]',
                $model->cnt_id,
                Countries::getCountriesList(),
                ['class' => 'form-control']
            );

            ?>
        </div>
        <div class='formBlock' style='width: 150px;'>User ID
            <?php
            echo Html::input(
                'text',
                'SendLogTask3Search[usr_id]',
                $model->usr_id,
                ['class' => 'form-control']
            );

            ?>
        </div>
        <div style="float: left; margin: 18px 0px 0px 10px;">
            <input type="submit" style="width: 100px;" class="form-control btn-primary" value="Submit"/>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

    <div style="margin: 5px; clear: both;">
        <div class='blockLeft' style="padding-top:5px;">Calculate aggregate data:</div>
        <div class='blockLeft'>
            <?= Html::a(
                'Go',
                '/send-log-aggregated/calculate-aggregated',
                ['class' => 'form-control btn-info']
            );?>
        </div>
    </div>

</div>
