<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "send_log".
 *
 * @property int $log_id
 * @property string $usr_id
 * @property string $num_id
 * @property string $log_message
 * @property int $log_success
 * @property string $log_created
 */
class SendLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'send_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usr_id', 'num_id', 'log_success'], 'required'],
            [['usr_id', 'num_id', 'log_success'], 'integer'],
            [['log_created'], 'safe'],
            [['log_message'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'log_id' => 'Log ID',
            'usr_id' => 'Usr ID',
            'num_id' => 'Num ID',
            'log_message' => 'Log Message',
            'log_success' => 'Log Success',
            'log_created' => 'Log Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::class, ['usr_id' => 'usr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNumber()
    {
        return $this->hasOne(Numbers::class, ['num_id' => 'num_id']);
    }

    public function getAggregated()
    {
        $sql = '
            SELECT l.usr_id, n.cnt_id, DATE_FORMAT(l.log_created, "%Y-%m-%d") AS logag_date,
                COUNT(IF(l.log_success = 1, 1, NULL)) AS logag_successed,
                COUNT(IF(l.log_success = 0, 1, NULL)) AS logag_failed
            FROM ' . self::tableName() . ' as l
                LEFT JOIN ' . Numbers::tableName() . ' AS n ON n.num_id = l.num_id
            GROUP BY DATE_FORMAT(l.log_created, "%Y-%m-%d"), l.usr_id, l.log_success, n.cnt_id
            ORDER BY DATE_FORMAT(l.log_created, "%Y-%m-%d") ASC
        ';

        return self::getDb()->createCommand($sql)->queryAll();
    }
}
