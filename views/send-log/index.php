<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SendLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Send Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="send-log-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Send Log', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'log_id',
            'usr_id',
            'num_id',
            'log_message',
            'log_success',
            //'log_created',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
