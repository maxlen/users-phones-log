<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class SendLogTask3Search extends SendLogAggregated
{
    const DEF_DAYS_CNT = 2;

    // vars for filter
    public $dateFrom = null;
    public $dateTo = null;

    public $cntId = null;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['logag_id', 'usr_id', 'cnt_id', 'logag_successed', 'logag_failed'], 'integer'],
            [['dateFrom', 'dateTo', 'logag_successed_sum', 'logag_failed_sum'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SendLogAggregated::find()->select(
            [
                'logag_date',
                'logag_successed_sum' => 'sum(logag_successed)',
                'logag_failed_sum' => 'sum(logag_failed)',
            ]
        )->orderBy(['logag_date' => SORT_DESC])->groupBy(['logag_date']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            $query->andWhere(['>=', 'logag_date', self::getMaxDate()]);
            return $dataProvider;
        }

        if ($this->dateFrom != '') {
            $query->andWhere(['>=', 'logag_date', $this->dateFrom]);
        } else {
            $query->andWhere(['>=', 'logag_date', self::getMaxDate()]);
        }

        if ($this->dateTo != '') {
            $query->andWhere(['<=', 'logag_date', $this->dateTo]);
        }

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'usr_id' => $this->usr_id,
            'cnt_id' => $this->cnt_id,
        ]);

        return $dataProvider;
    }

    public static function getMaxDate()
    {
        $result = self::getDb()->createCommand(
            "SELECT MAX(logag_date) AS maxDate FROM " . self::tableName()
        )->queryOne();

        return date(
            'Y-m-d',
            strtotime("-" . self::DEF_DAYS_CNT . " day", strtotime($result['maxDate']))
        );
    }
}
