<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SendLogAggregated */

$this->title = 'Create Send Log Aggregated';
$this->params['breadcrumbs'][] = ['label' => 'Send Log Aggregateds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="send-log-aggregated-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
