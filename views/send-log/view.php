<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SendLog */

$this->title = $model->log_id;
$this->params['breadcrumbs'][] = ['label' => 'Send Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="send-log-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->log_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->log_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'log_id',
            'usr_id',
            'num_id',
            'log_message',
            'log_success',
            'log_created',
        ],
    ]) ?>

</div>
