<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "numbers".
 *
 * @property int $num_id
 * @property string $cnt_id
 * @property string $num_number
 * @property string $num_created
 */
class Numbers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'numbers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cnt_id', 'num_number'], 'required'],
            [['cnt_id', 'num_number'], 'integer'],
            [['num_created'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'num_id' => 'Num ID',
            'cnt_id' => 'Cnt ID',
            'num_number' => 'Num Number',
            'num_created' => 'Num Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Countries::class, ['cnt_id' => 'cnt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSendLog()
    {
        return $this->hasMany(SendLog::class, ['log_id' => 'num_id']);
    }
}
