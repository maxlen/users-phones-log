<?php

use app\models\SendLog;
use yii\db\Migration;

/**
 * Class m180517_115405_create_table_send_log
 */
class m180517_115405_create_table_send_log extends Migration
{
    private $table = '{{%send_log}}';

    private $optString = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

    private $filePath = "@app/migrations/data/send_log.csv";

    public function safeUp()
    {
        $this->createTable($this->table, [
            'log_id' => $this->primaryKey(),
            'usr_id' => $this->integer()->unsigned()->notNull(),
            'num_id' => $this->integer()->unsigned()->notNull(),
            'log_message' => $this->string()->defaultValue(''),
            'log_success' => $this->tinyInteger(1)->notNull(),
            'log_created' => $this->dateTime(),
        ], $this->optString);

        $this->createIndex('usr_id', $this->table, 'usr_id');
        $this->createIndex('num_id', $this->table, 'num_id');
        $this->createIndex('log_success', $this->table, 'log_success');
        $this->createIndex('log_created', $this->table, 'log_created');

        echo PHP_EOL . "Fill table {$this->table} started ...";
        $fillResult = $this->fillTable();
        echo PHP_EOL . "Finished";
        return $fillResult;
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }

    private function fillTable(): bool
    {
        if (!is_file(Yii::getAlias($this->filePath))) {
            return false;
        }

        $dataForInsert = [];

        foreach (file(Yii::getAlias($this->filePath)) as $line) {
            $dataRow = explode(',', $line);
            $dataForInsert[] = [$dataRow[1], $dataRow[2], $dataRow[3], $dataRow[4], $dataRow[5]];
        }
        try {
            SendLog::getDb()->createCommand()->batchInsert(
                SendLog::tableName(),
                ['usr_id', 'num_id', 'log_message', 'log_success', 'log_created'],
                $dataForInsert
            )->execute();
        } catch (Exception $e) {
            return false;
        }

        return true;
    }
}
