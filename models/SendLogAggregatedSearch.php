<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SendLogAggregated;

/**
 * SendLogAggregatedSearch represents the model behind the search form of `app\models\SendLogAggregated`.
 */
class SendLogAggregatedSearch extends SendLogAggregated
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['logag_id', 'usr_id', 'cnt_id', 'logag_successed', 'logag_failed'], 'integer'],
            [['logag_date', 'logag_created'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SendLogAggregated::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'logag_id' => $this->logag_id,
            'usr_id' => $this->usr_id,
            'cnt_id' => $this->cnt_id,
            'logag_successed' => $this->logag_successed,
            'logag_failed' => $this->logag_failed,
            'logag_date' => $this->logag_date,
            'logag_created' => $this->logag_created,
        ]);

        return $dataProvider;
    }
}
