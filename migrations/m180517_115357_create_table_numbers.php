<?php

use app\models\Numbers;
use yii\db\Migration;

/**
 * Class m180517_115357_create_table_numbers
 */
class m180517_115357_create_table_numbers extends Migration
{
    private $table = '{{%numbers}}';
    private $optString = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
    private $itemsData = [
        [
            'cnt_id' => 233,
            'num_number' => 380994370398,
        ],
        [
            'cnt_id' => 1,
            'num_number' => 180100000001,
        ],
        [
            'cnt_id' => 100,
            'num_number' => 123123123123,
        ],
    ];

    public function safeUp()
    {
        $this->createTable($this->table, [
            'num_id' => $this->primaryKey(),
            'cnt_id' => $this->integer()->unsigned()->notNull(),
            'num_number' => $this->bigInteger()->unsigned()->notNull(),
            'num_created' => $this->dateTime(),
        ], $this->optString);

        $this->createIndex('cnt_id', $this->table, 'cnt_id');
        $this->createIndex('num_created', $this->table, 'num_created');

        return $this->fillTable();
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }

    private function fillTable(): bool
    {
        $dateTime = date("Y-m-d H:i:s");

        foreach ($this->itemsData as $itemData) {
            $itemData['num_created'] = $dateTime;
            if (!(new Numbers($itemData))->save()) {
                return false;
            }
        }
        return true;
    }
}
