<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Task3';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php if (Yii::$app->session->hasFlash('messText')): ?>
    <div class="alert alert-success" role="alert">
        <?= Yii::$app->session->getFlash('messText');?>
    </div>
<?php endif;?>

<div class="send-log-aggregated-index">
    <?php echo $this->render(
        '_form_task3',
        [
            'model' => $searchModel,
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
        ]
    ); ?>

    <div style="clear: both; padding-top: 5px;">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute' => 'logag_date',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::a($model->logag_date, ['/send-log-aggregated', 'SendLogAggregatedSearch[logag_date]' => $model->logag_date]);
                    },
                ],
                'logag_date',
                'logag_successed_sum',
                'logag_failed_sum',
                //'logag_created',

//            ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
