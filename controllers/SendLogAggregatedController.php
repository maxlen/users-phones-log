<?php

namespace app\controllers;

use app\models\SendLogTask3Search;
use Yii;
use app\models\SendLogAggregated;
use app\models\SendLogAggregatedSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SendLogAggregatedController implements the CRUD actions for SendLogAggregated model.
 */
class SendLogAggregatedController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SendLogAggregated models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SendLogAggregatedSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SendLogAggregated model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SendLogAggregated model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SendLogAggregated();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->logag_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SendLogAggregated model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->logag_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SendLogAggregated model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    public function actionTask3()
    {
        if (!empty(Yii::$app->request->queryParams)) {
            if (isset(Yii::$app->request->queryParams['SendLogTask3Search']['dateFrom'])) {
                $dateFrom = Yii::$app->request->queryParams['SendLogTask3Search']['dateFrom'];
            }
            if (isset(Yii::$app->request->queryParams['SendLogTask3Search']['dateTo'])) {
                $dateTo = Yii::$app->request->queryParams['SendLogTask3Search']['dateTo'];
            }
            if (!empty(Yii::$app->request->queryParams['SendLogTask3Search']['cntId'])) {
                $cntId = Yii::$app->request->queryParams['SendLogTask3Search']['cntId'];
            }
        }

        $searchModel = new SendLogTask3Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('task3', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dateFrom' => $dateFrom ?? SendLogTask3Search::getMaxDate(),
            'dateTo' => $dateTo ?? null,
            'cntId' => $cntId ?? null,
        ]);
    }

    /**
     * Finds the SendLogAggregated model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SendLogAggregated the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SendLogAggregated::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionCalculateAggregated($from = null, $to = null)
    {
        $model = new SendLogAggregated();
        $model->calculateAggregated();

        $this->redirect('/send-log-aggregated/task3');
    }
}
