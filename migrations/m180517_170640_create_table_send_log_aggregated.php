<?php

use yii\db\Migration;

/**
 * Class m180517_170640_create_table_send_log_aggregated
 */
class m180517_170640_create_table_send_log_aggregated extends Migration
{
    private $table = '{{%send_log_aggregated}}';
    private $optString = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'logag_id' => $this->primaryKey(),
            'usr_id' => $this->integer()->unsigned()->notNull(),
            'cnt_id' => $this->integer()->unsigned()->notNull(),
            'logag_successed' => $this->integer()->notNull(),
            'logag_failed' => $this->integer()->notNull(),
            'logag_date' => $this->date(),
            'logag_created' => $this->dateTime(),
        ], $this->optString);

        $this->createIndex('usr_id', $this->table, 'usr_id');
        $this->createIndex('cnt_id', $this->table, 'cnt_id');
        $this->createIndex('logag_date', $this->table, 'logag_date');
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
