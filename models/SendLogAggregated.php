<?php

namespace app\models;

use PHPUnit\Framework\Warning;
use Yii;
use yii\db\Exception;

/**
 * This is the model class for table "send_log_aggregated".
 *
 * @property int $logag_id
 * @property string $usr_id
 * @property string $cnt_id
 * @property int $logag_successed
 * @property int $logag_failed
 * @property string $logag_date
 * @property string $logag_created
 */
class SendLogAggregated extends \yii\db\ActiveRecord
{
    const ZERO_DATE = "0000-00-00";

    public $logag_successed_sum;
    public $logag_failed_sum;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'send_log_aggregated';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usr_id', 'cnt_id', 'logag_successed', 'logag_failed'], 'required'],
            [['usr_id', 'cnt_id', 'logag_successed', 'logag_failed'], 'integer'],
            [['logag_date', 'logag_created'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'logag_id' => 'ID',
            'usr_id' => 'Usr ID',
            'cnt_id' => 'Cnt ID',
            'logag_successed' => 'Successed',
            'logag_failed' => 'Failed',
            'logag_date' => 'Date',
            'logag_created' => 'Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::class, ['usr_id' => 'usr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Countries::class, ['cnt_id' => 'cnt_id']);
    }

    public function calculateAggregated()
    {
        $newAggregatedData = (new SendLog())->getAggregated();

        $now = date("Y-m-d");

        // here we can use bulkInsert or Like me (each by model)
        foreach ($newAggregatedData as $aggrItem) {
            $rowForSave = $this->getAlreadyRow($aggrItem);
            if (empty($rowForSave)) {
                $rowForSave = new SendLogAggregated($aggrItem);
            } else {
                $rowForSave->logag_successed = $aggrItem['logag_successed'];
                $rowForSave->logag_failed = $aggrItem['logag_failed'];
            }

            $rowForSave->save();

            // clean all data from send_log. Except today data.
            if ($aggrItem['logag_date'] != $now) {
                SendLog::deleteAll('log_created <= :logag_date', [':logag_date' => $aggrItem['logag_date'] . " 23:59:59"]);
            }
        }

        Yii::$app->session->setFlash('messText', 'Send Log calculated and saved to arrgegate storage');
    }

    private function getAlreadyRow($data)
    {
        return SendLogAggregated::find()->where(
            [
                'usr_id' => $data['usr_id'],
                'cnt_id' => $data['cnt_id'],
                'logag_date' => $data['logag_date'],
            ]
        )->one();
    }

    /**
     * Task2
     * getting information from the new aggregated log
     */
    public function getData($dateFrom, $dateTo, int $cntId = null, int $usrId = null)
    {
        $query = self::find()->where(
            'logag_date >= :dateFrom AND logag_date <= :dateTo',
            [':dateFrom' => $dateFrom, ':dateTo' => $dateTo]
        );

        if (!empty($cntId)) {
            $query->andWhere('cnt_id = :cntId', [':cntId' => $cntId]);
        }

        if (!empty($usrId)) {
            $query->andWhere('usr_id = :ustId', [':usrId' => $usrId]);
        }

        return $query->all();
    }
}
