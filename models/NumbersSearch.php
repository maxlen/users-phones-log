<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Numbers;

/**
 * NumbersSearch represents the model behind the search form of `app\models\Numbers`.
 */
class NumbersSearch extends Numbers
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['num_id', 'cnt_id', 'num_number'], 'integer'],
            [['num_created'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Numbers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'num_id' => $this->num_id,
            'cnt_id' => $this->cnt_id,
            'num_number' => $this->num_number,
            'num_created' => $this->num_created,
        ]);

        return $dataProvider;
    }
}
