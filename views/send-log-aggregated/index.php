<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SendLogAggregatedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Send Log Aggregateds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="send-log-aggregated-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'logag_id',
            'usr_id',
            'cnt_id',
            'logag_successed',
            'logag_failed',
            'logag_date',
            //'logag_created',

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
