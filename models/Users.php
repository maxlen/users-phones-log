<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $usr_id
 * @property string $usr_name
 * @property int $usr_active
 * @property string $usr_created
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usr_name'], 'required'],
            [['usr_active'], 'integer'],
            [['usr_created'], 'safe'],
            [['usr_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'usr_id' => 'Usr ID',
            'usr_name' => 'Usr Name',
            'usr_active' => 'Usr Active',
            'usr_created' => 'Usr Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSendLog()
    {
        return $this->hasMany(SendLog::class, ['usr_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSendLogAggr()
    {
        return $this->hasOne(SendLogAggregated::class, ['usr_id' => 'usr_id']);
    }
}
