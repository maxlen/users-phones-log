<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SendLog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="send-log-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'usr_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'num_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'log_message')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'log_success')->textInput() ?>

    <?= $form->field($model, 'log_created')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
