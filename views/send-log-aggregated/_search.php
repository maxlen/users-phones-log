<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SendLogAggregatedSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="send-log-aggregated-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'logag_id') ?>

    <?= $form->field($model, 'usr_id') ?>

    <?= $form->field($model, 'cnt_id') ?>

    <?= $form->field($model, 'logag_amount_type') ?>

    <?= $form->field($model, 'logag_amount') ?>

    <?php // echo $form->field($model, 'logag_date') ?>

    <?php // echo $form->field($model, 'logag_created') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
