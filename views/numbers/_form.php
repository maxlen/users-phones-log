<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Numbers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="numbers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cnt_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'num_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'num_created')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
