<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $cnt_id
 * @property int $cnt_code
 * @property string $cnt_title
 * @property string $cnt_created
 */
class Countries extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'countries';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cnt_code', 'cnt_title'], 'required'],
            [['cnt_code'], 'integer'],
            [['cnt_created'], 'safe'],
            [['cnt_title'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cnt_id' => 'Cnt ID',
            'cnt_code' => 'Cnt Code',
            'cnt_title' => 'Cnt Title',
            'cnt_created' => 'Cnt Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNumbers()
    {
        return $this->hasMany(Numbers::class, ['cnt_id' => 'id']);
    }

    public static function getCountriesList()
    {
        $result = ['' => 'all'];
        foreach (self::find()->orderBy(['cnt_title' => SORT_ASC])->all() as $item) {
            $result[$item->cnt_id] = $item->cnt_title;
        }

        return $result;
    }
}
