<?php

use app\models\User;
use app\models\Users;
use yii\db\Migration;

/**
 * Class m180517_115338_create_table_users
 */
class m180517_115338_create_table_users extends Migration
{
    private $table = '{{%users}}';
    private $optString = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
    private $usersData = [
        [
            'usr_name' => 'Ivan',
            'usr_active' => 1,
        ],
        [
            'usr_name' => 'Chris',
            'usr_active' => 0,
        ],
        [
            'usr_name' => 'Josef',
            'usr_active' => 1,
        ],
    ];

    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'usr_id' => $this->primaryKey(),
                'usr_name' => $this->string(50)->notNull(),
                'usr_active' => $this->tinyInteger(1)->unsigned()->defaultValue(1),
                'usr_created' => $this->dateTime(),
            ],
            $this->optString
        );

        $this->createIndex('usr_active', $this->table, 'usr_active');
        $this->createIndex('usr_created', $this->table, 'usr_created');
        
        return $this->fillTable();
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }

    private function fillTable(): bool
    {
        $dateTime = date("Y-m-d H:i:s");

        foreach ($this->usersData as $userData) {
            $userData['usr_created'] = $dateTime;
            if (!(new Users($userData))->save()) {
                return false;
            }
        }
        return true;
    }
}
